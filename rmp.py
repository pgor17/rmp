#!/usr/bin/python

import sys
from treeop import Tree, str2tree, node2label, randtree, ns, pt
import getopt,itertools
from score import rmp, aggrtreeepi 

verbose=""
printstreewithscores=0

def pc(c): return "".join(sorted(c))

def readintervalfile(filename):

    try:
        t=[ l.strip() for l in open(filename,"r").readlines() if len(l.strip()) and l.strip()[0]!='#' ]
    except IOError as e:
        print filename," I/O error({0}): {1}".format(e.errno, e.strerror)
        quit()
    
    if t[0][0].isdigit():
        gtnum=int(t[0])
        offset=1
    else: 
        gtnum=1
        offset=0
    
    gtrees=[]

    for i in xrange(gtnum):
        if verbose.find("0") <> -1:
            print "Processing line ", t[i+offset]
        gtrees.append(Tree(str2tree(t[i+offset])))
    st=Tree(str2tree(t[i+1+offset]))

    stnodespostorder=st.root.nodes()

    oldgtnum=-1
   
    for i in xrange(i+2+offset,len(t)):
    
        gtnum,gc,sc,l=t[i].replace("+"," +").split(";")
        l=int(l)
        gtnum=int(gtnum)

        if oldgtnum!=gtnum:
            gt=gtrees[gtnum]
            gtnodespostorder=gt.root.nodes()
            oldgtnum=gtnum
        
        if gc[0].isdigit():
            g=gtnodespostorder[int(gc)]
            s=stnodespostorder[int(sc)]
            l=stnodespostorder[l]

        else:
            gc=gc.strip().split()
            sc=sc.strip().split()


            g=gt.findnodeplus(gc)
            s=st.findnodeplus(sc)


        if not g:
            raise Exception("Incorrect interval cluster "+str(gc))
        if not s:
            raise Exception("Incorrect interval cluster "+str(sc))

        g.setinterval(s,l)

        
    return gtrees,st


def readgs(filename):

    t=[ l.strip() for l in open(filename,"r").readlines() if len(l.strip()) and l.strip()[0]!='#' ]
    if verbose.find("0") <> -1:
        print t
    #print str2tree(t[0])
    return Tree(str2tree(t[0])),Tree(str2tree(t[1]))



def usage():
    print """
Usage: [-g GTREE|GTFILE ] [-s SPECIESTREE|STFILE ] [-v d|a] FILE FILE ...

 -g TREE        - to define input gene tree(s)
 -s TREE        - to define input species tree(s)
 -v d           - debug verbose
 -v a           - print species tree with aggregated mp/epi stats 
 -v q           - do not print the output cost
 FILE           - joint format: NUMBER EOLN GeneTree_1 EOLN ... GeneTree_NUMBER EOLN SpeciesTree

Print MP score:
> rmp.py -g '((((c,c),b),b),(c,(a,(b,b)))))' -s '(a,(b,c))'

Print the species tree with attributes: 
- mpepi - episodes at a node
- mppathscore - the score of a path (at leaves only)
- mp - the score of a tree (at root only)
- epiaggr - frequency dictionary: the number of episodes k - the number of gene trees having k episodes at a given node  

> rmp.py -g '((((c,c),b),b),(c,(a,(b,b)))))' -s '(a,(b,c))' -va
(a mpepi=0 mppathscore=2,(b epiaggr='1:1' mpepi=1 mppathscore=3 maxpath=1 mark=maxpathmark,c epiaggr='1:1' mpepi=1 mppathscore=3 maxpath=1 mark=maxpathmark) mpepi=0) epiaggr='2:1' mpepi=2 mp=3

Many gene tree analysis
> rmp.py guigo.txt -va

"""
    

def readgtreefile(gtreefile):
    gtrees=[]
    for g in open(gtreefile).readlines():
         if g and g[0]!="#":
            gtrees.append(Tree(str2tree(g.strip())))
    return gtrees



def ftread(a):
    try:
        return [ Tree(str2tree(l)) for l in open(a,'r') if l.strip() and l.strip()[0]!='#']
    except Exception as e:
        return [ Tree(str2tree(a))]        \
    


def main():
     # parse command line options

    try:
        opts, args = getopt.getopt(sys.argv[1:], "s:g:v:p:mc:q", ["help", "output="])
    except getopt.GetoptError as err:
        print str(err) 
        usage()
        sys.exit(2)
    if len(sys.argv)==1:
        usage()
        sys.exit(1)

    model=3 
    stree = None
    gtrees = []
    gt=st=None
    global verbose,printstreewithscores,delnodes

    outputfile=None
    runmer=1 # default run
    gtreefile=None
    firstgtrees=-1
    numtrees=-1
    ppheight=0
    delnodes=""
    printstreewithscores=0
    st=[]


    for o, a in opts:
        if o == "-s": st.extend(ftread(a))        
        elif o == "-v": verbose=str(a)        
        elif o == "-p":
            gtree,stree=a.strip().split(" ")
            gtrees.append(Tree(str2tree(gtree)))
            st.append(Tree(str2tree(stree)))
        elif o == "-r":
            n=int(a)
            #print randtree(n)
            gtrees.append(Tree(randtree(n)))
            st.append(Tree(randtree(n)))
            
        elif o == "-g": gtrees.extend(ftread(a))            

        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        else:
            assert False, "unhandled option"    
    
    for l in args:
        gtrees,st=readintervalfile(l)
        st=[st]

    
    if not st or not gtrees:
        print "Both trees have to be defined"
        usage()
        sys.exit(3)

    
    for s in st:        
        rmp(s,gtrees,verbose,0)


   
    return 0
        

if __name__ == "__main__":
    main()
