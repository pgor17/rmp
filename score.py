
from treeop import Tree

def dupcmp(d1,d2):
    if d1.map==d2.map: return d2.duplevel-d1.duplevel
    return d1.map.prefixnum-d2.map.prefixnum

def mpbypath(l):
    s=0
    while l:
        s+=l.epi
        l=l.parent
    return s

verbose=''

def absorb(g,s):
    if 'd' in verbose:
        print "absorb",g,s

    # remove from the current level
    g.map.cnt[g.duplevel]-=1
    if not g.map.cnt[g.duplevel]: g.map.epi-=1

    # add at next level 
    g.map=s
    g.duplevel=1
    g.map.cnt[1]+=1

    # and correct duplevel for parents if needed
    while g.parent.dup and g.parent.map==s and g.duplevel==g.parent.duplevel:
        gp=g.parent
        s.cnt[gp.duplevel]-=1               
        gp.duplevel+=1
        s.cnt[gp.duplevel]+=1               
        g=gp
       
def duppathlen(g,s):        
    if g.dup and g.map==s: return 1+duppathlen(g.parent,s)
    return 0

def LastLevel(a,s,last):
    if a==s: return last
    if a.epi>0: return LastLevel(a.parent,s,a)
    return  LastLevel(a.parent,s,last)


def trytoabsorb(g,stroot):  
    s=g.parent.map    
    if g.map==s: return None

    if g.parent.dup:      
        dpl=duppathlen(g.parent,s)
        if dpl<s.epi: 
            if 'd' in verbose: print "A1",g.id,g,g.parent,g.parent.duplevel,g.parent.dup
            return (g,s)
            
    last=LastLevel(g.map.parent,s,None)
    if last:
        if 'd' in verbose: print "A2"
        return (g,last)        

   


def aggrtreeepi(st,gtrees,mp):
    for n in st.nodes: n.aggrepi={}

    for l in st.leaves():
        l.mppathscore=mpbypath(l)
        l.maxpath=mp==l.mppathscore

    for n in st.nodes: n.mpepi=n.epi # copy old mpepi values

    for gt in gtrees:
        dupnodes=[ n for n in gt.nodes if n.dup ]
        for n in st.nodes: n.epi=0
        calcepiduplevel(dupnodes)   
        # aggregate
        for n in st.nodes:
            if not n.epi: continue
            if n.epi not in n.aggrepi: n.aggrepi[n.epi]=0
            n.aggrepi[n.epi]+=1


    #print tree
    def pepi(n):
        if n.aggrepi: 
            a="epiaggr='%s'"%n.aggrepi      
            a=" "+a.replace(" ","").replace("{","").replace("}","")
        else: a=''
        a+=' mpepi=%d'%n.mpepi
        if not n.leaf():        
            return "(%s,%s)%s"%(pepi(n.l),pepi(n.r),a)      
        mpinfo=" mppathscore=%d"%n.mppathscore
        if n.maxpath: mpinfo+=" maxpath=1 mark=maxpathmark"
        return "%s%s%s"%(n.clusterleaf,a,mpinfo)

    if hasattr(st,"outgroup"): n=st.root.l
    else: n=st.root

    return pepi(n)+" mp=%d"%mp


# calc. duplevel and epi from map
def calcepiduplevel(dupnodes):
    for n in dupnodes: n.duplevel=0
    for n in dupnodes: # prefix F   
        if n.l.dup and n.l.map==n.map:
            n.duplevel=n.l.duplevel
        if n.r.dup and n.r.map==n.map:
            n.duplevel=max(n.duplevel,n.r.duplevel)
        n.duplevel+=1
        n.map.epi=max(n.map.epi,n.duplevel)



def rmp(st,gtrees,scoreverbose,msopt):
    global verbose
    verbose=scoreverbose
    dupnodes=[]
    gtnodes=[]

    if not hasattr(st,"outgroup"):
        # add outgroup if not present

        # find outgroup label   
        outgroup="@"
        if "@" in st.root.cluster:
            i=1
            while "@%d"%i in st.root.cluster: i+=1
            outgroup="@%d"%i


        # insert outgroup into trees
        gtrees=[ Tree((g.src,outgroup)) for g in gtrees ]
        st=Tree((st.src,outgroup))
        st.outgroup=outgroup

    for gt in gtrees:
        gt.setlcamapping(st)
        for g in gt.nodes: gtnodes.append(g)

    for i,g in enumerate(gtnodes): g.id=i

    if 'd' in verbose:
        for g in gtnodes:
            print "GN",g.id,"par=%d"%g.parent.id if g.parent else "root",
            if not g.leaf():
                print "l=%d r=%d"%(g.l.id,g.r.id),
            print "Node=%s"%g
            


    for n in gtnodes: # prefix          
        n.map=n.lcamap
        if n.leaf(): n.spec=True
        else: n.spec=n.r.map!=n.map and n.l.map!=n.map 
        n.dup=not n.spec                    
        if n.dup: 
            dupnodes.append(n)

    for i,n in enumerate(st.root.nodespreorder()):
        n.epi=0
        n.prefixnum=i
        
    calcepiduplevel(dupnodes)
    

    #for gt in gtrees: setduplevels(gt.root,-1)

    for n in st.nodes: n.cnt=[0]*(n.epi+1)
    for g in dupnodes: g.map.cnt[g.duplevel]+=1         
    
    dupnodes=sorted(dupnodes,cmp=dupcmp)

    def pp():
        if 'd' in verbose:
            for i in dupnodes: print "D",i.id,i.clurepr(),"map=%s"%i.map.clurepr(),"duplevel=%d"%i.duplevel
            print   
            for s in st.nodes: print "S",s.clurepr(),"epi=%d"%s.epi,"cnt=%s"%s.cnt

    pp()

    if msopt:
        # variant absorb at max s first
        #rmp.py -m -g "((((a,b),b),b),(b,(b,b)))" -s "(a,b)" 
        for s in st.root.nodespreorder():
            
            for d in dupnodes: # one loop is sufficient; dupnodes are sorted
                abp=trytoabsorb(d,st.root)                    
                if abp and abp[1]==s:
                    # absorb
                    absorb(*abp)
                    if 'd' in verbose:
                        print "--------------",d.lcamap.parent,"absorbed at",s
                        pp()
        
            


    else: 
        for d in dupnodes:
            abp=trytoabsorb(d,st.root)
            if abp:
                absorb(*abp)
                if 'd' in verbose:
                    print "--------------",d.lcamap.parent,"absorbed at",abp[1]
                    pp()
            else: 
                if 'd' in verbose:
                    print "not absorbed",d,d.id

    for l in st.root.leaves(): l.mpscore=mpbypath(l) 
    mpscore=max(l.mpscore for l in st.root.leaves())

    if 'a' in verbose:
        print aggrtreeepi(st,gtrees,mpscore)
    else:
        if not 'q' in verbose:
            print mpscore

    return mpscore

