# rmp

# Computing the MP score from a collection of rooted gene trees and a rooted species tree.

Usage: rmp.py [-v d|a] [-g GTREE|GTFILE ] [-s SPECIESTREE|STFILE ] FILE FILE ...

* -g TREE        - to define input gene tree(s)
* -s TREE        - to define input species tree(s)
* -v d           - debug verbose
* -v a           - print species tree with aggregated mp/epi stats 
* -v q           - do not print the output cost
* FILE           - joint format: NUMBER EOLN GeneTree_1 EOLN ... GeneTree_NUMBER EOLN SpeciesTree

Print MP score:
---------------

rmp.py -g '((((c,c),b),b),(c,(a,(b,b)))))' -s '(a,(b,c))'

3

Full report emebdded into the species tree: 
-------------------------------------------

* mpepi - episodes at a node
* mppathscore - the score of a path (at leaves only)
* mp - the score of a tree (at root only)
* epiaggr - frequency dictionary: the number of episodes k - the number of gene trees having k episodes at a given node  

rmp.py -va -g '((((c,c),b),b),(c,(a,(b,b)))))' -s '(a,(b,c))'

(a mpepi=0 mppathscore=2,(b epiaggr='1:1' mpepi=1 mppathscore=3 maxpath=1 mark=maxpathmark,c epiaggr='1:1' mpepi=1 mppathscore=3 maxpath=1 mark=maxpathmark) mpepi=0) epiaggr='2:1' mpepi=2 mp=3

Single file input: many gene trees + one species tree
-----------------------

rmp.py -va guigo2.txt

(prot mpepi=0 mppathscore=3,(fung mpepi=0 mppathscore=3,((chlo mpepi=0 mppathscore=4,embr mpepi=0 mppathscore=4) mpepi=0,(arth mpepi=0 mppathscore=5,((acoe mpepi=0 mppathscore=5,anne mpepi=0 mppathscore=5) mpepi=0,(echi mpepi=0 mppathscore=5,(chon mpepi=0 mppathscore=5,(oste mpepi=0 mppathscore=5,(amph mpepi=0 mppathscore=6 maxpath=1 mark=maxpathmark,(moll mpepi=0 mppathscore=6 maxpath=1 mark=maxpathmark,((mamm mpepi=0 mppathscore=6 maxpath=1 mark=maxpathmark,(aves mpepi=0 mppathscore=6 maxpath=1 mark=maxpathmark,rept mpepi=0 mppathscore=6 maxpath=1 mark=maxpathmark) mpepi=0) mpepi=0,agna mpepi=0 mppathscore=6 maxpath=1 mark=maxpathmark) mpepi=0) mpepi=0) epiaggr='1:4' mpepi=1) mpepi=0) mpepi=0) mpepi=0) mpepi=0) epiaggr='1:2' mpepi=1) epiaggr='1:3' mpepi=1) mpepi=0) epiaggr='1:16,2:2,3:2' mpepi=3 mp=6
